#ShareLinks

ShareLinks是用来给多个用户提供共享网页链接的平台，用户通过注册和登陆到系统之后，可以发布优秀的网页链接，其他用户则可以通过ShareLinks访问其网页，获取所需信息，这是一个共享知识系统，目的是共同学习，以合作的方式学习。

* 项目基于Meteor框架
* 数据库使用MongoDB
* 网页样式使用Bootstrap3
* 路由使用Meteor组件iron:router

此项目也是学习Meteor的结果，目前项目正在完善中，已部署到阿里云ECS
访问地址：

http://blog.reeveslee.com:50

运行结果预览：

![运行结果](https://s3.amazonaws.com/discovermeteor/screenshots/3-1.png)
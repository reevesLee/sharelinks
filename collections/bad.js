/**
 * Created by reeves on 2017/3/2.
 */
Bad = new Mongo.Collection('bad');

Bad.allow({
    update: function () {
        return true;
    }
});

Meteor.methods({
    badInsert: function (badAttributes) {
        check(this.userId, String);
        check(badAttributes, {
            postId: String
        });
        var user = Meteor.user();
        var badExist = Bad.findOne({postId: badAttributes.postId, userId: user._id});
        if (badExist)
            throw new Meteor.Error('invalid-star', 'You have already advised to delete the post');

        var bad = _.extend(badAttributes, {
            userId: user._id,
            time: new Date()
        });

        // create the star, save the id
        bad._id = Bad.insert(bad);

        // now create a notification, informing the user that there's been a comment
        // createCommentNotification(comment);
        return bad._id;
    }
});
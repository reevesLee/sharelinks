/**
 * Created by reeves on 2017/3/2.
 */
Star = new Mongo.Collection('star');

Star.allow({
    update: function () {
        return true;
    }
});

Meteor.methods({
    starInsert: function (starAttributes) {
        check(this.userId, String);
        check(starAttributes, {
            postId: String
        });
        var user = Meteor.user();
        var starExist = Star.findOne({postId: starAttributes.postId, userId: user._id});
        if (starExist)
            throw new Meteor.Error('invalid-star', 'You have already give a star to this post');

        var star = _.extend(starAttributes, {
            userId: user._id,
            time: new Date()
        });

        // create the star, save the id
        star._id = Star.insert(star);
        // 更新帖子的评论数
        Posts.update(star.postId, {$inc: {star: 1}});

        // now create a notification, informing the user that there's been a comment
        // createCommentNotification(comment);
        return star._id;
    }
});
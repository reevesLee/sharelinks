/**
 * Created by reeves on 2017/1/25.
 */
Posts = new Mongo.Collection('posts');

Posts.allow({
    update: function (userId, post) {
        // return ownsDocument(userId, post);
        return true;
    },
    remove: function (userId, post) {
        return ownsDocument(userId, post);
    }
});

Posts.deny({
    update: function (userId, post, fieldNames) {
        // 只能更改如下两个字段：
        return (_.without(fieldNames, 'url', 'title','category','star').length > 0);
    }
});

// Posts.deny({
//     update: function (userId, post, fieldNames, modifier) {
//         var errors = validatePost(modifier.$set);
//         return errors.title || errors.url;
//     }
// });

Meteor.methods({
    postInsert: function (postAttributes) {
        check(this.userId, String);
        check(postAttributes, {
            title: String,
            url: String,
            category:String,
            star:Number
        });

        var errors = validatePost(postAttributes);
        if (errors.title || errors.url)
            throw new Meteor.Error('invalid-post', "你必须为你的帖子填写标题和 URL");

        var postWithSameLink = Posts.findOne({url: postAttributes.url});
        if (postWithSameLink) {
            return {
                postExists: true,
                _id: postWithSameLink._id
            }
        }

        var user = Meteor.user();
        var post = _.extend(postAttributes, {
            userId: user._id,
            author: user.username,
            submitted: new Date(),
            commentsCount: 0
        });

        var postId = Posts.insert(post);

        return {
            _id: postId
        };
    }
});

validatePost = function (post) {
    var errors = {};
    if (!post.title)
        errors.title = "请填写标题";
    if (!post.url)
        errors.url = "请填写 URL";
    return errors;
}
/**
 * Created by reeves on 2017/2/24.
 */
Feedback = new Mongo.Collection('feedback');

Feedback.allow({
    update:function () {
        return true;
    }
});


Meteor.methods({
    feedbackInsert: function (feedbackAttributes) {
        check(this.userId, String);
        check(feedbackAttributes, {
            content: String,
            reply: Match.Any
        });
        var user = Meteor.user();
        feedback = _.extend(feedbackAttributes, {
            userId: user._id,
            name: user.username,
            time: new Date()
        });

        // create the comment, save the id
        feedback._id = Feedback.insert(feedback);
        return feedback._id;
    }
});

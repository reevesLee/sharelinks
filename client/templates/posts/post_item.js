/**
 * Created by reeves on 2017/1/25.
 */
Template.postItem.helpers({
    domain: function () {
        var a = document.createElement('a');
        a.href = this.url;
        return a.hostname;
    },
    ownPost: function () {
        return this.userId === Meteor.userId();
    },
    commentsCount: function () {
        var result = Posts.findOne({_id: this._id});
        if (result) {
            return result.commentsCount;
        } else {
            return 0;
        }
    }
});
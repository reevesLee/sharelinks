/**
 * Created by reeves on 2017/1/25.
 */
Template.postPage.helpers({
    comments: function () {
        return Comments.find({postId: this._id});
    }
});

// 事件监听处理
Template.postPage.events({
    'click #starBtn': function (e) {
        var starNumber = Template.instance().data.star + 1;
        var currentPostId = this._id;

        var starProperties = {
            postId: currentPostId
        }

        Meteor.call('starInsert', starProperties, function (error, result) {
            // 向用户显示错误信息并终止
            if (error)
                return throwError(error.reason);
        });
    },
    'click #badBtn': function (e) {
        var currentPostId = this._id;
        alert('你的建议已收到，感谢你的参与~');

        var badProperties = {
            postId: currentPostId
        }

        Meteor.call('badInsert', badProperties, function (error, result) {
            // 向用户显示错误信息并终止
            if (error)
                return throwError(error.reason);
        });
    }
});
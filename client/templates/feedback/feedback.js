/**
 * Created by reeves on 2017/2/23.
 */
Template.feedbackItem.helpers({
    isAdmin: function () {
        var loginUser = Meteor.user();
        return loginUser.username == "admin"; // 判断是否是管理员
    }
});

Template.feedback.events({
    'submit form[name=comment]': function (e, template) {
        // 添加返回
        var $body = $(e.target).find('[name=body]');
        var feedback = {
            content: $body.val(),
            reply: {}
        };

        Meteor.call('feedbackInsert', feedback, function (error, feedbackId) {
            if (error) {
                throwError(error.reason);
            } else {

            }
        });
    },
    'submit form[name=reply]': function (e, template) {
        // 添加回复
        var feedback_id = this._id;
        var userId = Template.instance().data.userId; // 用户_id
        var $body = $(e.target).find('[name=feedbackBody]');

        var postProperties = {
            reply: {
                content: $body.val(),
                time: new Date()
            }
        };

        Feedback.update(feedback_id, {$set: postProperties}, function (error) {
            if (error) {
                console.log('回复失败!');
                throwError(error.reason);
            } else {

                console.log('回复成功!');
            }
        })
    }
});
/**
 * Created by reeves on 2017/1/27.
 */
Template.header.events({
    'keyup #searchInput': function (e, template) {
        var keywords = $(e.target).val(); // 搜索框输入的字符

        Router.go('/posts/search?keywords=' + keywords);
    }
});
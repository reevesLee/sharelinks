/**
 * Created by reeves on 2017/1/25.
 */
ownsDocument = function(userId, doc) {
    return doc && doc.userId === userId;
}
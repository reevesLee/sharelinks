/**
 * Created by reeves on 2017/1/25.
 */
Meteor.publish('posts', function (options) {
    check(options, {
        sort: Object,
        limit: Number
    });
    return Posts.find({}, options);
});

Meteor.publish('search', function (keywords, options) {
    check(keywords, String);
    check(options, {
        sort: Object
    });
    var pattern = new RegExp("^.*" + keywords + ".*$", "i");
    return Posts.find({title: pattern}, options);
});

// 文章分类列表
// Meteor.publish('categorys',function () {
//     var distinctEntries = _.uniq(Posts.find({}, {
//         sort: {category: 1}, fields: {category: true}
//     }).fetch().map(function(x) {
//         return x.category;
//     }), true);
//
//     return distinctEntries;
// });

Meteor.publish('singlePost', function (id) {
    check(id, String);
    return Posts.find(id);
});

// 发布特定分类下的帖子
Meteor.publish('categoryPost', function (categoryName) {
    check(categoryName, String);
    return Posts.find({category: categoryName});
});

Meteor.publish('comments', function (postId) {
    check(postId, String);
    return Comments.find({postId: postId});
});

Meteor.publish('notifications', function () {
    return Notifications.find();
});

// 发布分类信息
Meteor.publish('category',function () {
    return Category.find();
});

// 发布反馈信息
Meteor.publish('feedback',function () {
    return Feedback.find();
});